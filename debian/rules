#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_APPEND=-Wl,--as-needed

DEB_HOST_ARCH_OS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)
DEB_UPSTREAM_VERSION=$(shell dpkg-parsechangelog | \
			sed -rne 's,^Version: ([^-]+).*,\1,p')

%:
	dh $@

override_dh_auto_configure:
	# Disable ALSA support on non-Linux archs
	if [ $(DEB_HOST_ARCH_OS) = linux ]; then \
		dh_auto_configure -- --disable-g15; \
	else \
		dh_auto_configure -- --disable-g15 --without-alsa; \
	fi

override_dh_auto_build:
	# Generate icons/mangler-icons.h
	cd icons/ && make mangler-icons.h

	# Generate src/manglerui.h
	cd src/ && echo -n "char ManglerUI[] = " > manglerui.h && \
	cat mangler.ui  | sed 's/"/\\"/g' | sed 's/^/"/' | \
	sed 's/$$/\\n"/' >> manglerui.h && echo ";" >> manglerui.h

	dh_auto_build

override_dh_auto_test:

get-orig-source:
	# Remove existing mangler directory and tarball, if existing
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)
	rm -f mangler_$(DEB_UPSTREAM_VERSION).orig.tar.gz
	# Fetch latest upstream tarball
	uscan --noconf --force-download --download-current-version \
	--destdir=.
	# Unpack and remove tarball
	tar -xzf mangler-$(DEB_UPSTREAM_VERSION).tar.gz
	rm -f mangler-$(DEB_UPSTREAM_VERSION).tar.gz
	# Remove potentially non-DFSG-compliant (and other unwanted) files
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/debian/
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/rpm/
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/android/
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/iphone/
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/libventrilo3/codec-test/
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/icons/mangler-icons.h
	rm -rf mangler-$(DEB_UPSTREAM_VERSION)/src/manglerui.h
	# Repack tarball
	tar -czf mangler_$(DEB_UPSTREAM_VERSION).orig.tar.gz mangler-\
	$(DEB_UPSTREAM_VERSION)/
